#!usr/bin/env-sh

echo "VERSION BUILD"

VER=$(cat VERSION).${BUILD_NUMBER}

mkdir -p reports

echo '{"key":"value"}' > ./reports/results_ver-${VER}.json
